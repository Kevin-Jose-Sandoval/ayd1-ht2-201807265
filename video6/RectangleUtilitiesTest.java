package video6;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class RectangleUtilitiesTest {

	@Test
	public void testGetPerimeter() {
		int height = 4;
		int width = 6;
		int expected = 20; // 2 * (4+6)
		int actual = RectangleUtilities.getPerimeter(height, width);
		
		// expected: what you believe the value should be
		// actual: the value your code actually produces
		assertEquals(expected, actual);
	}

}
