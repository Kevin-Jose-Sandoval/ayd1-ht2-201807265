package video8;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

public class Theory {

	@BeforeAll
	public static void setup() {
		System.out.print("first");
	}
	
	@AfterAll
	public static void teardown() {
		System.out.print("last");
	}
	
	@Test
	public void test1() {
		System.out.print("now running test 1");
	}
	
	@Test
	public void test2() {
		System.out.print("now running test 2");
	}

}
