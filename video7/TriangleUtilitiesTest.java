package video7;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class TriangleUtilitiesTest {

	@Test
	void test() {
		assertFalse(TriangleUtilities.isEquilateral(2, 3, 4));
		assertFalse(TriangleUtilities.isEquilateral(2, 3, 4));
		assertTrue(TriangleUtilities.isEquilateral(4, 4, 4));
	}

}
