package video5;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class Practice02 {
	int x = 2;
	int y = 2;
	int z = 3;
	
	boolean a = (x == y);
	boolean b = (y == z);
	
	//write a test with an assert that shows
	// a is true
	@Test
	void test1() {
		assertTrue(a);
	}
	
	// write a test method with an assert that shows
	// b is false
	@Test
	void test2() {
		assertFalse(b);
	}
}
