package video5;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class Practice01 {
	int x = 2;
	int y = 2;
	int z = 3;
	
	//write a test method with an assert that shows
	// x and y are equal to each other
	@Test
	void test1() {
		assertEquals(x, y);
	}
	
	// write a test method with an assert that shows
	// y and z are not equal to each other
	@Test
	void test2() {
		assertNotEquals(y, z);
	}

}
