package video4;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class AssertTrueTheory {

	@Test
	void test1() {
		assertTrue(true);
	}
	
	@Test
	void test2() {
		assertTrue(2 == 3);
	}
	
	@Test
	void test3() {
		assertTrue("abc".length() == 3);
	}

}
