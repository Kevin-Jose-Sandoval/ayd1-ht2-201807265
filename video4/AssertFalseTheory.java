package video4;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class AssertFalseTheory {

	@Test
	void test() {
		assertFalse(false);
		assertFalse(1 == 2);
		assertFalse("abc".length() == 4);
	}

}
