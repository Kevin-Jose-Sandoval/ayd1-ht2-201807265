# AyD1-HT2-201807265

JUnit
- A framework for unit testing
- What is unit testing?
  - Test-driven development
  
Advantages
- Separate your tests from your code; no noeed for main() methods.
- Keep a growing library of tests.
- Make sure new code does not break existing code (regression testing).

# Curso completado
<p align="center"><img src="./img/course.png"  height="500"/></p>

<br/>

# Quiz 1
<p align="center"><img src="./img/quiz1.png"  height="500"/></p>

<br/>

# Quiz 2
<p align="center"><img src="./img/quiz2.png"  height="500"/></p>


