package video10;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class TriangleUtilitiesTest {

	@Test
	void testIsIsosceles() {
		int sideA = 3;
		int sideB = 2;
		int sideC = 3;
		assertTrue(TriangleUtilities.isIsosceles(sideA, sideB, sideC));
	}

}
